package br.com.devlucaspires.desafio_uol.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 27/11/17.
 */

public class User {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("html_url")
    @Expose
    public String htmlUrl;
    @SerializedName("avatar_url")
    @Expose
    public String avatarUrl;
    @SerializedName("bio")
    @Expose
    public String bio;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("buckets_count")
    @Expose
    public Integer bucketsCount;
    @SerializedName("comments_received_count")
    @Expose
    public Integer commentsReceivedCount;
    @SerializedName("followers_count")
    @Expose
    public Integer followersCount;
    @SerializedName("followings_count")
    @Expose
    public Integer followingsCount;
    @SerializedName("likes_count")
    @Expose
    public Integer likesCount;
    @SerializedName("likes_received_count")
    @Expose
    public Integer likesReceivedCount;
    @SerializedName("projects_count")
    @Expose
    public Integer projectsCount;
    @SerializedName("rebounds_received_count")
    @Expose
    public Integer reboundsReceivedCount;
    @SerializedName("shots_count")
    @Expose
    public Integer shotsCount;
    @SerializedName("teams_count")
    @Expose
    public Integer teamsCount;
    @SerializedName("can_upload_shot")
    @Expose
    public Boolean canUploadShot;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("pro")
    @Expose
    public Boolean pro;
    @SerializedName("buckets_url")
    @Expose
    public String bucketsUrl;
    @SerializedName("followers_url")
    @Expose
    public String followersUrl;
    @SerializedName("following_url")
    @Expose
    public String followingUrl;
    @SerializedName("likes_url")
    @Expose
    public String likesUrl;
    @SerializedName("shots_url")
    @Expose
    public String shotsUrl;
    @SerializedName("teams_url")
    @Expose
    public String teamsUrl;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;


    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getBio() {
        return bio;
    }

    public String getLocation() {
        return location;
    }

    public Integer getBucketsCount() {
        return bucketsCount;
    }

    public Integer getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    public Integer getFollowersCount() {
        return followersCount;
    }

    public Integer getFollowingsCount() {
        return followingsCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public Integer getLikesReceivedCount() {
        return likesReceivedCount;
    }

    public Integer getProjectsCount() {
        return projectsCount;
    }

    public Integer getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    public Integer getShotsCount() {
        return shotsCount;
    }

    public Integer getTeamsCount() {
        return teamsCount;
    }

    public Boolean getCanUploadShot() {
        return canUploadShot;
    }

    public String getType() {
        return type;
    }

    public Boolean getPro() {
        return pro;
    }

    public String getBucketsUrl() {
        return bucketsUrl;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public String getLikesUrl() {
        return likesUrl;
    }

    public String getShotsUrl() {
        return shotsUrl;
    }

    public String getTeamsUrl() {
        return teamsUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }


}
