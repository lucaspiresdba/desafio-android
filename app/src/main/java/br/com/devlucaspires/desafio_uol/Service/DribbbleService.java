package br.com.devlucaspires.desafio_uol.Service;

import java.util.List;

import br.com.devlucaspires.desafio_uol.Model.Shot;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by lucas on 27/11/17.
 */

public interface DribbbleService {

    String END_POINT = "https://api.dribbble.com/v1/";
    String Access_Token = "29ccdf67b671b9e97e120e8024811af3e7a3ac6fdc095d10d838547d94fb59f9";
    String API_OAUTH_HEADER = "Authorization: Bearer " + Access_Token;

    @GET("shots")
    @Headers(API_OAUTH_HEADER)
    Observable<List<Shot>> getShotsList(@Query("page") int page, @Query("sort") String sort);

    class buildRetrofit {
        private static DribbbleService service;
        public static DribbbleService getInstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(END_POINT).build();
                service = retrofit.create(DribbbleService.class);
                return service;
            } else {
                return service;
            }
        }

    }
}
