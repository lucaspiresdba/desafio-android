package br.com.devlucaspires.desafio_uol.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.devlucaspires.desafio_uol.R;
import br.com.devlucaspires.desafio_uol.Classes.ShotClass;
import br.com.devlucaspires.desafio_uol.ShotActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucas on 27/11/17.
 */

public class ShotAdapter extends RecyclerView.Adapter<ShotAdapter.Holder> {

    private List<ShotClass> shotArray = new ArrayList<>();
    private Context context;


    public ShotAdapter(List<ShotClass> dados, Context context) {
        this.shotArray = dados;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.shot_adapter, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ShotClass shotDados = shotArray.get(position);
        holder.comnt.setText(String.valueOf(shotDados.getComnt()));
        holder.likes.setText(String.valueOf(shotDados.getLikes()));
        holder.views.setText(String.valueOf(shotDados.getViews()));
        Picasso.with(context).load(shotDados.getUrlFoto()).into(holder.shot);
        holder.nome = shotDados.getNome();
        holder.descr = shotDados.getDescricao();
        holder.urlImg = shotDados.getUrlFoto();
    }

    @Override
    public int getItemCount() {
        return shotArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_views)
        TextView views;

        @BindView(R.id.txt_likes)
        TextView likes;

        @BindView(R.id.txt_cmnt)
        TextView comnt;

        @BindView(R.id.img_adapter)
        ImageView shot;
        private String nome, descr, urlImg;

        @BindView(R.id.card)
        CardView cardView;

        @BindView(R.id.detalhes)
       TextView detalhes;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            detalhes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent chamarShotAct = new Intent(context, ShotActivity.class);
                    chamarShotAct.putExtra("autor", nome);
                    chamarShotAct.putExtra("descricao", descr);
                    chamarShotAct.putExtra("foto", urlImg);
                    context.startActivity(chamarShotAct);
                }
            });
        }
    }

}
