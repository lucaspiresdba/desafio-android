package br.com.devlucaspires.desafio_uol.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucas on 27/11/17.
 */


public class Shot {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("images")
    @Expose
    public Images images;

    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("views_count")
    @Expose
    public Integer viewsCount;
    @SerializedName("likes_count")
    @Expose
    public Integer likesCount;
    @SerializedName("comments_count")
    @Expose
    public Integer commentsCount;
    @SerializedName("attachments_count")
    @Expose
    public Integer attachmentsCount;
    @SerializedName("rebounds_count")
    @Expose
    public Integer reboundsCount;
    @SerializedName("buckets_count")
    @Expose
    public Integer bucketsCount;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("html_url")
    @Expose
    public String htmlUrl;
    @SerializedName("attachments_url")
    @Expose
    public String attachmentsUrl;
    @SerializedName("buckets_url")
    @Expose
    public String bucketsUrl;
    @SerializedName("comments_url")
    @Expose
    public String commentsUrl;
    @SerializedName("likes_url")
    @Expose
    public String likesUrl;
    @SerializedName("projects_url")
    @Expose
    public String projectsUrl;
    @SerializedName("rebounds_url")
    @Expose
    public String reboundsUrl;
    @SerializedName("animated")
    @Expose
    public Boolean animated;
    @SerializedName("tags")
    @Expose
    public List<String> tags = null;
    @SerializedName("user")
    @Expose
    public User user;

    public Images getImages() {
        return images;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getViewsCount() {
        return viewsCount;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public Integer getAttachmentsCount() {
        return attachmentsCount;
    }

    public Integer getReboundsCount() {
        return reboundsCount;
    }

    public Integer getBucketsCount() {
        return bucketsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getAttachmentsUrl() {
        return attachmentsUrl;
    }

    public String getBucketsUrl() {
        return bucketsUrl;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public String getLikesUrl() {
        return likesUrl;
    }

    public String getProjectsUrl() {
        return projectsUrl;
    }

    public String getReboundsUrl() {
        return reboundsUrl;
    }

    public Boolean getAnimated() {
        return animated;
    }

    public List<String> getTags() {
        return tags;
    }

    public User getUser() {
        return user;
    }

    public class Images {

        @SerializedName("hidpi")
        @Expose
        public Object hidpi;
        @SerializedName("normal")
        @Expose
        public String normal;
        @SerializedName("teaser")
        @Expose
        public String teaser;

        public Object getHidpi() {
            return hidpi;
        }

        public String getNormal() {
            return normal;
        }

        public String getTeaser() {
            return teaser;
        }
    }

}
