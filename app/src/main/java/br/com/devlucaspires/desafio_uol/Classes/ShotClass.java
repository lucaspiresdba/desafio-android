package br.com.devlucaspires.desafio_uol.Classes;

/**
 * Created by lucas on 27/11/17.
 */

public class ShotClass {

    private String urlFoto;
    private int views;
    private int likes;
    private int comnt;
    private String nome;
    private String descricao;

    public ShotClass(int viewsC, int likesC, int comtnC, String urlimgC, String nome, String descC) {
        this.comnt = comtnC;
        this.likes = likesC;
        this.views = viewsC;
        this.urlFoto = urlimgC;
        this.nome = nome;
        this.descricao = descC;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public int getViews() {
        return views;
    }

    public int getLikes() {
        return likes;
    }

    public int getComnt() {
        return comnt;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }
}
