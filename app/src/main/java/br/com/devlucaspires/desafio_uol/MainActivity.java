package br.com.devlucaspires.desafio_uol;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


import br.com.devlucaspires.desafio_uol.Adapter.ShotAdapter;
import br.com.devlucaspires.desafio_uol.Classes.EndlessRecyclerViewScrollListener;
import br.com.devlucaspires.desafio_uol.Classes.ShotClass;
import br.com.devlucaspires.desafio_uol.Model.Shot;
import br.com.devlucaspires.desafio_uol.Service.DribbbleService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucas on 27/11/17.
 */

public class MainActivity extends Activity {

    private static final String RECENT = "recent";
    private static final String COMMENTS = "comments";
    private static final String VIEWS = "views";
    private static final String POP = "popularity";

    @BindView(R.id.rb_pop)
    RadioButton rb_pop;

    @BindView(R.id.rb_recent)
    RadioButton rb_recent;

    @BindView(R.id.rb_comments)
    RadioButton rb_comments;

    @BindView(R.id.rb_views)
    RadioButton rb_views;

    @BindView(R.id.RecyclerView)
    RecyclerView recyclerView;

    private EndlessRecyclerViewScrollListener scrollListener;
    private ArrayList<ShotClass> shotArray = new ArrayList<ShotClass>();
    private ShotAdapter adapter = new ShotAdapter(shotArray, MainActivity.this);
    private int pagina = 1;
    private ProgressDialog pg;
    private String ordernar_por = POP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        pg = new ProgressDialog(MainActivity.this);
        pg.setMessage("Carregando...");
        pg.setIndeterminate(true);
        pg.hide();

        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        scrollListener = new EndlessRecyclerViewScrollListener(lm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(pagina, ordernar_por);
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        loadNextDataFromApi(pagina, ordernar_por);
    }

    public void loadNextDataFromApi(int offset, String sort) {
        pg.show();
        Observable<List<Shot>> shots = DribbbleService.buildRetrofit.getInstance().getShotsList(offset, sort);
        shots.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Shot>>() {
                    @Override
                    public void onCompleted() {
                        pagina++;
                        pg.hide();
                    }

                    @Override
                    public void onError(Throwable e) {
                        pg.hide();
                        Toast.makeText(MainActivity.this, "Nâo foi possível obter os Shots", Toast.LENGTH_SHORT).show();
                        Log.i("TAG", e.getMessage());
                    }

                    @Override
                    public void onNext(List<Shot> shots) {
                        try {
                            for (int i = 0; i < shots.size(); i++) {
                                shotArray.add(new ShotClass(shots.get(i).getViewsCount(), shots.get(i).getLikesCount(), shots.get(i).getCommentsCount(),
                                        shots.get(i).images.getNormal(), shots.get(i).user.getName(), shots.get(i).getDescription()));
                                adapter.notifyItemInserted(shotArray.size());
                            }
                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
                            e.getStackTrace();
                        }

                    }
                });
    }

    public void limparAdp() {
        shotArray.clear();
        adapter.notifyDataSetChanged();
        scrollListener.resetState();
        pagina = 1;
    }

    @OnClick({R.id.rb_pop, R.id.rb_comments, R.id.rb_recent, R.id.rb_views})
    public void ordenacaoShots(Button button) {
        switch (button.getId()) {
            case R.id.rb_pop:
                ordernar_por = POP;
                break;
            case R.id.rb_comments:
                ordernar_por = COMMENTS;
                break;
            case R.id.rb_recent:
                ordernar_por = RECENT;
                break;
            case R.id.rb_views:
                ordernar_por = VIEWS;
        }
        limparAdp();
        loadNextDataFromApi(pagina, ordernar_por);
    }
}
