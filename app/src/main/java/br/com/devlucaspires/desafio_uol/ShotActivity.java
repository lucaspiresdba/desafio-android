package br.com.devlucaspires.desafio_uol;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotActivity extends Activity {

    @BindView(R.id.txt_autor_name)
    TextView autor;

    @BindView(R.id.txt_desc_shot)
    TextView desc;

    @BindView(R.id.imageView)
    ImageView foto;
    private String tratString;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot);
        ButterKnife.bind(this);

        tratString = getIntent().getStringExtra("descricao");
        try {
            tratString = tratString.replaceAll("\\<(/?[^\\>]+)\\>", " ").replaceAll("\\s+", " ");
        } catch (NullPointerException e) {
            tratString = "Shot sem descrição";
        }

        desc.setText(tratString);
        autor.setText(getIntent().getStringExtra("autor"));
        Picasso.with(this).load(getIntent().getStringExtra("foto")).into(foto);
    }
}
